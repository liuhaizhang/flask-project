from werkzeug.security import generate_password_hash, check_password_hash


def set_password(password):
    '''
    :param password: 带加密的密码，如123456
    :return: 加密的密码：pbkdf2:sha256:150000$0koyI6Eb$cff6e1b193381f5891fc1cf7b87b1b4dab33869aa5490a7f935579e47c7666cf
    '''
    password = generate_password_hash(password)
    return password


def check_password(password, pwhash):
    '''
    :param password: 字符串密码，如123456
    :param pwhash: 加密后的密码，存在用户表中的hash值
    :return: Ture or False
    '''
    return check_password_hash(password=password, pwhash=pwhash)