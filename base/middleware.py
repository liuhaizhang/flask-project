from flask import request,jsonify
from ext import db

#响应前使用的中间件【跨域】
def after_request(resp):
    '1、中间件解决跨域'
    #允许所有源来访问
    resp.headers['Access-Control-Allow-Origin'] = '*'
    #设置跨域访问的请求方式
    resp.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,OPTIONS,DELETE,PATCH'
    #设置跨域访问可以使用的请求头信息
    resp.headers['Access-Control-Allow-Headers'] = 'x-requested-with,content-type,token'

    '2、错误信息处理'
    code = resp.status_code
    response_data = resp.get_json()
    if code!=200:
        print(code,type(code),response_data)
    if code == 400:
        ret_dic = {
            'code':400,
            'error':''
        }

        #1、修改flask-restful 校验数据时，返回的错误信息
        error_data = response_data.get('message')
        if error_data:
            if isinstance(error_data,list):
                error_list = [f'{k}:{v}' for k ,v in error_data.items()]
                ret_dic['code']=416
                ret_dic['error'] = error_list
            else:
                ret_dic['error'] = error_data
            #重新设置好响应的数据
            resp.set_data(jsonify(ret_dic).data)
            #重新设置好响应的状态码
            resp.status_code = 200

    elif code == 429:
        print(response_data,'429')
        resp.set_data(jsonify({'code':400,'error':'访问频率过快'}).data)
        resp.status_code = 200

    elif code == 404:
        print(resp,404)
        resp.set_data(jsonify({'code':400,'error':'资源或路由不存在'}).data)
        resp.headers['Content-Type']='application/json' #flask返回404错误时，使用的text/html ，改成json格式
    elif code == 405:
        resp.set_data(jsonify({'code': 400, 'error':'未开放该请求方式' }).data)

    elif code ==500:
        print(response_data,'500错误')
        message = response_data.get('message')
        resp.set_data(jsonify({'code':500,'error':message}).data)

    '统一关闭数据库连，在响应关闭db连接'
    db.session.close()
    return resp

#请求得到视图前触发：统一content-type 数据的存放位置
def before_request():
    #非get请求，可以在请求中携带数据的
    if request.method !='GET' and request.content_type:
        print(request.content_type, '请求体类型')
        #根据content-type: 将请求体数据取出，统一存放到request.data中
        data = {}
        #json数据
        if request.content_type =='application/json':
            data = request.get_json() or {}
        #表单数据
        elif 'multipart/form-data' in request.content_type:
            if request.form:
                data = {k:v for k,v in request.form.items()}
        #类表单数据
        elif request.content_type=='application/x-www-form-urlencoded':
            if request.form:
                data = {k:v for k,v in request.form.items()}

        #在视图函数中，就可以使用request.data获取请求体的数据了，一般就无需理会content-type的类型了
        if data:
            request.data = data
