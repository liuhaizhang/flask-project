from flask import views
from flask_restful import Resource
from .authentication import login_required
from flask import request

#所有的视图都继承这个
class ResourceBase(Resource):
    decorators = [login_required]
    method_decorators = [login_required]
    def __new__(cls, *args, **kwargs):
        cls.method_decorators = cls.decorators
        return super().__new__(cls)

class ViewBase(views.MethodView):
    decorators = [login_required]