from flask import jsonify
from threading import RLock
_lock = RLock()
#给flask的原生视图类使用的响应体
class _Response:
    instance = None
    def __new__(cls, *args, **kwargs):
        if cls.instance:
            return cls.instance

        with _lock:
            if cls.instance:
                return cls.instance
            cls.instance = object.__new__(cls)
        return cls.instance
    def __call__(self, code=200,msg='操作成功',error=None,data=None,pagenation = None):
        dic = {
            'code':code,
            'msg':msg
        }
        #1、处理分页数据
        if pagenation:
            return jsonify(pagenation)

        #2、处理错误信息
        if error:
            dic.pop('msg')
            dic['error'] = error
            if type(error) == str:
                dic['code']=400
            elif type(error) == list:
                dic['code']=416
            else:
                dic['code'] = 400

        #3、处理有携带数据
        if data:
            dic['data'] = data
        return jsonify(dic)

NewResponse = _Response()