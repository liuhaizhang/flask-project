import os
import logging
import time
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler


# log配置，实现日志自动按日期生成日志文件
def make_dir(make_dir_path):
    path = make_dir_path.strip()
    if not os.path.exists(path):
        os.makedirs(path)


def getLogHandlerFile():
    # 存放日志的目录名，创建在项目根目录下
    log_dir_name = "logs"
    # 文件名，以日期作为文件名
    log_file_name = 'logger-' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + '.log'
    # 构建项目根目录的路径
    log_file_folder = os.path.abspath(
        os.path.join(os.path.dirname(__file__), os.pardir))
    # 如果logs目录不存在，就创建该目录
    make_dir(os.path.join(log_file_folder, log_dir_name))
    # 构建日志文件的路径
    log_file_str = os.path.join(log_file_folder, log_dir_name, log_file_name)

    '1、基本配置'
    # 默认日志等级的设置
    logging.basicConfig(level=logging.INFO)
    # 设置日志的格式:发生时间,日志等级,日志信息文件名,      函数名,行数,日志信息
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)s - %(message)s')

    '2、基于文件大小切割日志'
    # 创建日志记录器，指明日志保存路径,每个日志的大小，保存日志的上限
    file_log_handler = RotatingFileHandler(log_file_str, maxBytes=1024 * 1024 * 5, backupCount=10, encoding='UTF-8')
    file_log_handler.setFormatter(formatter)  # 设置日志的格式
    file_log_handler.setLevel(logging.INFO)  # 设置日志等级

    return file_log_handler  # 基于文件大小分割日志的方案

def getLogHanderTime():
    # 存放日志的目录名，创建在项目根目录下
    log_dir_name = "logs"
    # 文件名，以日期作为文件名
    log_file_name = 'logger-' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + '.log'
    # 构建项目根目录的路径
    log_file_folder = os.path.abspath(
        os.path.join(os.path.dirname(__file__), os.pardir))
    # 如果logs目录不存在，就创建该目录
    make_dir(os.path.join(log_file_folder, log_dir_name))
    # 构建日志文件的路径
    log_file_str = os.path.join(log_file_folder, log_dir_name, log_file_name)

    '1、基本配置'
    # 默认日志等级的设置
    logging.basicConfig(level=logging.DEBUG)
    # 设置日志的格式:发生时间,日志等级,日志信息文件名,      函数名,行数,日志信息
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)s - %(message)s')


    '2、基于时间间隔切割日志文件'

    '''
    # 往文件里写入指定间隔时间自动生成文件的Handler
    # 实例化TimedRotatingFileHandler
    # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
    # S 秒
    # M 分
    # H 小时
    # D 天
    # 'W0'-'W6' 每星期（interval=0时代表星期一：W0）
    # midnight 每天凌晨
    '''
    file_log_time_handler = TimedRotatingFileHandler(filename=log_file_str, when='D', backupCount=10,
                                                     encoding='utf-8')  # 日志处理器
    file_log_time_handler.setFormatter(formatter)  # 日志格式
    file_log_time_handler.setLevel(logging.DEBUG)  # 日志等级

    return file_log_time_handler  # 基于文件大小分割日志的方案




if __name__ == '__main__':
    from flask import current_app
    current_app.looger.error('手动写日志')