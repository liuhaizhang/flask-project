from flask import  render_template, request, jsonify
from flask_socketio import SocketIO, send, emit, join_room
from ext import socketio
from flask_socketio import Namespace
'''
一、非群聊功能，前端需要实时更新某些数据使用
1、返回html页面
2、主动发送websocket到后端，后端返回数据给请求的用户
3、调用某个视图函数，在视图函数中，给所有连接推送新的数据
'''

class TotalWebsocketNamespace(Namespace):
    def on_handle_data(self,data):
        print(data, '接收浏览器发送的数据')
        # 1、给发送给后端的websocket，发送数据，单独给这个websocket发送
        # socketio.emit('handle_data', {'data':'返回的数据','type':'user','msg':'单独返回'})
        emit('handle_data', {'data': '返回的数据', 'type': 'user', 'msg': '主动请求时，返回的数据'})

    def on_connect(self):
        print('connect连接')
        token = request.args.get('token')
        sid = request.sid
        print(request.args, 'args')
        # print('连接的sid',request.sid)

        if token == '123456':
            emit('success', '验证token成功')
            join_room('default')  # 加入到默认的房间中了
            # 表明连接成功
        else:
            print('token验证失败')
            # 阻止连接
            return False

