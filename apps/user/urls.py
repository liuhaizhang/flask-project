# from ext import api
from . import views
from flask_restful import Api
from flask import Blueprint
#使用蓝图的方式来注册api
user_bp = Blueprint('user',__name__,url_prefix='/user')
api = Api(user_bp)

#注册功能
api.add_resource(views.RegisterResource,'/register',endpoint='user-register')
#登录功能
api.add_resource(views.LoginResource,'/login',endpoint = 'user-login')
#测试token功能，该路由访问需要token
api.add_resource(views.HomeResource,'/home',endpoint='user-home')
#用户资源: get所有
api.add_resource(views.UserResource,'/info',endpoint='user-info')
#用户资源：单个的get/put/delete
api.add_resource(views.UserOneResource,'/info/<int:id>',endpoint = 'user-info-one')
#检测celery任务状态的接口
api.add_resource(views.CheckCeleryTaskResource,'/check/task',endpoint='user-check-task')