from flask_restful import fields, inputs
from flask_restful import reqparse

#注册用户时，使用的校验
register_parser = reqparse.RequestParser()
register_parser.add_argument('name',type=str,help='用户姓名必填',required=True,location = ['json','form'])
register_parser.add_argument('account',type=str,help='账户必填',required=True,location = ['json','form'])
register_parser.add_argument('password',type=inputs.regex(r'(\w|\d){6,12}'),help='密码6-12位长度',required=True,location = ['json','form'])
register_parser.add_argument('phoneNumber',type=inputs.regex(r'^1[345678]\d{9}$'),help='手机格式有问题',required=True,location = ['json','form'])

#用户登录时，使用的校验
login_parser = reqparse.RequestParser()
login_parser.add_argument('account',type=str,help='账户必填',required=True,location = ['json','form'])
login_parser.add_argument('password',type=inputs.regex(r'(\w|\d){6,12}'),help='密码6-12位长度',required=True,location = ['json','form'])

#用户表的序列化字典
user_fields = {
    'id':fields.Integer,
    'name':fields.String,
    'account':fields.String,
    'phoneNumber':fields.String
}