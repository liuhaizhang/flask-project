from ext import db

class UserModel(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(32),nullable=False)
    account = db.Column(db.String(32),nullable=False,unique=True,index=True)
    password = db.Column(db.String(256),nullable=False,index=True)
    phoneNumber = db.Column(db.String(15))
    picture = db.Column(db.String(256))
