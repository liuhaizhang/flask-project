from flask import Blueprint
from flask_restful import Api
from . import views

html_bp = Blueprint('html',__name__,url_prefix='/html')
api = Api(html_bp)

#对于返回html页面视图，使用蓝图
html_bp.add_url_rule('/index',view_func=views.IndexView.as_view('html-index'))
#对于标准的api视图，使用api
api.add_resource(views.IndexBroatResource,'/broat',endpoint='html-broat')