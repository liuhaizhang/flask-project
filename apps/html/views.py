from flask import request,render_template

from base.views import ViewBase,ResourceBase
from base.response import NewResponse
from ext import socketio


#返回html页面：测试websocket功能
class IndexView(ViewBase):
    decorators = []
    def get(self):
        return render_template('websocket/index.html')

#手动广播给websocket对象
class IndexBroatResource(ResourceBase):
    decorators = []
    def get(self):
        # 给房间名中的所有websocket，广播数据，告知所有连接去获取新的数据，
        socketio.emit('handle_data', {'data': '返回的数据', 'type': 'user', 'msg': '数据更新，广播到房间中返回的数据'}, room='default')
        return NewResponse(data={'msg':'hhhhhhh'})