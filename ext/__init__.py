from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_caching import Cache
from flask_socketio import SocketIO
from flask_limiter import Limiter
from .config import get_remote_address
from base.settings import REDIS_PORT,REDIS_HOST

db = SQLAlchemy() #数据库对象
cors = CORS() #跨域
cache = Cache() #缓存
socketio = SocketIO()#websocket对象
limiter = Limiter(
    key_func=get_remote_address, #以IP地址作为限制
    default_limits=['80/minute'], #访问频率控制
    storage_uri = f'redis://{REDIS_HOST}:{REDIS_PORT}', #使用redis数据库作为存储
    strategy='fixed-window' #滑动窗口
) #设置limiter对象，并设置一个默认的流量控制规则
