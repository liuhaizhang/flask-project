from flask import request
from base.settings import REDIS_HOST,REDIS_PORT,REDIS_PASSWORD
#跨域的配置
cors_config = {
    "origins": "*", #所有域都允许
    "expose_headers": ["Content-Type", "token","x-requested-with"], #跨域请求头允许的
    "methods": ["GET", "POST","PUT","PATCH","OPTIONS","DELETE"], #跨域允许的请求方式
    "supports_credentials": True, #允许在cookies跨域
}


#cache缓存配置-使用redis数据库
cache_config_redis = {
    'CACHE_TYPE':'redis',
    'CACHE_REDIS_HOST':REDIS_HOST,#redis的主机地址
    'CACHE_REDIS_PORT':REDIS_PORT,#redis的端口号
    # 'CACHE_REDIS_PASSWORD':REDIS_PASSWORD, #如果redis配置了密码
    'CACHE_REDIS_DB':0, #指定使用的redis的db，默认0-15
}

#cache缓存配置-使用内存
cache_config_mem = {
    'CACHE_TYPE':'simple'#使用内存作为cache
}

#流量控制，全局的限制规则
def get_remote_address():
    real_ip = request.headers.get('X-Real-IP')
    forwarded_for = request.headers.get('X-Forwarded-For')
    local_ip = request.remote_addr
    if forwarded_for:
        ip = forwarded_for.split(',')[0]
    elif real_ip:
        ip = real_ip
    else:
        ip = local_ip
    return ip