from flask_migrate import Manager
from apps import create_app
from ext import db,socketio
from flask_migrate import Migrate,MigrateCommand
from flask_socketio import join_room,emit
#导入模型类
from apps.user.models import *

app = create_app()
#应用于数据库用migrate管理
migrate = Migrate(app,db)

#flask2.x版本后，不再与flask-script兼容
# manager = Manager(app=app)
# manager.add_command('db',MigrateCommand)
# 启动 manager.run() #数据库迁移使用这个

if __name__ == '__main__':
    # app.run()
    socketio.run(app, debug=True,host='0.0.0.0',port=5000) #启动websocket