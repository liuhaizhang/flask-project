from celery import Celery,Task
from .celeryconfig import config,task_module
import sys
import os
'1、把flask项目路径添加到系统环境变量中'
project_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(project_path)

'''
2、创建celery应用对象
  'task'可以任务是该celery对象名字，用于区分celery对象
  broker是指定消息中间件
  backend是指定任务结果存储位置
  include是手动指定异步任务所在的模块的位置
'''
#创建celery异步对象
celery = Celery('task', broker=config.get('broker_url'), backend=config.get('result_backend'), include=task_module)
#导入一些基本配置
celery.conf.update(**config)

'3、给celery所有任务添加flask的应用上下文，在celery异步任务中就可以调用flask中的对象了'
class ContextTask(celery.Task):
    def __call__(self, *args, **kwargs):
        from apps import create_app
        app = create_app()
        with app.app_context():
            return self.run(*args, **kwargs)
celery.Task = ContextTask
