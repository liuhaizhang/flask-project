import time
# 导入celery对象app
from celery_task.celery import celery
import os
#导入发送邮件的模块
from base.email_ import send_email
from ext import cache


'''
所有异步任务：
1、没有返回值的，@app.task(ignore_result=True)
2、有返回值的任务，@app.task 默认就是(ignore_result=False)
'''


# 没有返回值，禁用掉结果后端
@celery.task
def send_email_task(receiver_email,code):  # 此时可以直接传邮箱，还能减少一次数据库的IO操作
    '''
    :param email: 接收消息的邮箱，用户的邮箱
    :return:
    '''
    # 启用线程发送邮件，此处最好加线程池
    ret = send_email(
        receiver_email=receiver_email,
        subject='登录验证码',
        message=f'您的验证码是：{code}',
    )
    return {'result':ret,receiver_email:code}

@celery.task
def cache_user_task():
    from apps.user.models import UserModel
    user = UserModel.query.all()
    lis = []
    for u in user:
        id = u.id
        name = u.name
        dic = {'id':id,'name':name}
        lis.append(dic)
        print(dic)
    cache.set('all-user-data',lis)
    return {'code':200,'msg':'查询数据成功'}

@celery.task
def test_check_fun():
    print('耗时开始:30秒')
    time.sleep(30)
    print('耗时结束...')
    return {'result':'异步任务执行后返回值','time':30}

@celery.task(ignore_result=True)
def save_file_task(file_bytes,file_path,delete_file_path=None):
    '''
    file_bytes: flask调用使用传递的文件字节流
    file_path: 文件保存的绝对路径
    1、对与路径的拼接和文件名重复的逻辑，通通在视图函数中处理
    2、在这里，只负责将文件保存到系统中，不管其他的逻辑
    '''
    #1、查看文件所在路径是否存在，不存在就创建
    file_path_dir = os.path.dirname(file_path)
    if not os.path.exists(file_path_dir):
        os.makedirs(file_path_dir)

    #2、保存文件
    with open(file_path, 'wb+') as f:
        f.write(file_bytes)

    #3、删除旧文件
    if delete_file_path:
        #想头像图片，一个用户只保存一个头像文件就可以了，删除之前旧的头像文件
        try:
            os.remove(delete_file_path)
        except Exception as _:
            print('删除失败')
            pass
