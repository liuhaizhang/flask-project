from celery_task.celery import celery
import time
'''
所有定时任务
'''

# 有返回值，返回值可以从结果后端中获取
@celery.task
def add_func(a, b):
    print('执行了加法函数',a+b)
    return a + b


# 不需要返回值，禁用掉结果后端
@celery.task(ignore_result=True)
def cache_user_func():
    print('all')


