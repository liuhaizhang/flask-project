import os
import uuid
from werkzeug.datastructures import FileStorage

def public_save_file(file:FileStorage,path:str):
    '''
    通用的文件保存方法
    file: 前端上传的文件
    path: 该文件要保存的路径
    '''
    file_suffix = file.name.split('.')[-1]
    file_name = str(uuid.uuid4())+'.'+file_suffix
    if not os.path.exists(path):
        os.makedirs(path)
    file_path = os.path.join(path, file_name)
    while True:
        if os.path.exists(file_path):
            file_name = str(uuid.uuid4()) + '.' + file_suffix
            file_path = os.path.join(path, file_name)
        else:
            break
    try:
        with open(file_path, 'wb+') as f:
            for chunk in file.chunks():
                f.write(chunk)
            f.close()
        return True
    except Exception as _:
        return False

#创建目录的方法
def create_dir_fun(path):
    if not os.path.exists(path):
        os.makedirs(path)