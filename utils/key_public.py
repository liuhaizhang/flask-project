import os

#根目录路径
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
#静态资源路径
STATIC_PATH = os.path.join(BASE_PATH,'static')


if __name__ == '__main__':
    print(STATIC_PATH)
    print(BASE_PATH)