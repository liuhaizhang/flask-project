###flask2.x版本前，数据库迁移
#1、数据库迁移
python app.py db init           #初始化，只执行一次
python app.py db migrate        #生成迁移文件
python app.py db upgrade        #执行迁移命令
#2、启动项目
python app.py runserver -h 0.0.0.0 -p 8000





###flask2.x以后数据库迁移
#1、数据库迁移
flask db init       #初始化，只执行一次
flask db migrate    #生成迁移文件
flask db upgrade    #执行迁移命令
#2、项目启动
flask run --host 0.0.0.0 --port 8000
#是否启动degbug模式
--debugger
--no-debugger